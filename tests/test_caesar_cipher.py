from coe_number.a_3 import caesarCipher
import unittest

class caesar_cipher(unittest.TestCase):
    
    def test_give(self):
        s='ll'
        k=0
        is_ll = caesarCipher(s,k)
        self.assertEqual(is_ll, 'll')
    
    def test_mm(self):
        s='mm'
        k=26
        is_mm = caesarCipher(s,k)
        self.assertEqual(is_mm, 'mm')    
    def test_aa(self):
        s='/+'
        k=1
        is_ = caesarCipher(s,k)
        self.assertEqual(is_, '/+')    

    def test_sometext(self):
        s="middle-Outz"
        k=2
        is_cd = caesarCipher(s,k)
        self.assertEqual(is_cd, "okffng-Qwvb")
    