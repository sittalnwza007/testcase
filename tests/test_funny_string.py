from coe_number.a_4 import funnyString
import unittest

class funny_stringtest(unittest.TestCase):
    def testisfunny(self) :
        Aphabet_lst_1=['a','b','d','e']
        is_funny = funnyString(Aphabet_lst_1)
        self.assertEqual(is_funny, 'Funny')
        
    def testisNotfunny(self) :
        Aphabet_lst_2=['b','c','x','z']
        is_Notfunny = funnyString(Aphabet_lst_2)
        self.assertEqual(is_Notfunny, 'Not Funny')