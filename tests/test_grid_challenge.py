from coe_number.a_5 import gridChallenge
import unittest

class grid_challenge_test(unittest.TestCase) :
    def test_grid_1(self) :
        grid=['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        is_YES = gridChallenge(grid)
        self.assertEqual(is_YES, "YES")

    def test_grid_2(self) :
        grid=['mpxz', 'abcd', 'wlmf']
        is_YES = gridChallenge(grid)
        self.assertEqual(is_YES, "NO")
    
    def test_grid_3(self) :
        grid=['abc', 'lmp', 'qrt']
        is_YES = gridChallenge(grid)
        self.assertEqual(is_YES, "YES")

    