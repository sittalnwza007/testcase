from coe_number.a_2 import alternatingCharacters
import unittest

class alternating_characters(unittest.TestCase):

    def testAAABBB(self) :
        k='AAABBB'
        is_4 = alternatingCharacters(k)
        self.assertEqual(is_4, 4)

    def testAAAA(self) :
        k='AAAA'
        is_3 = alternatingCharacters(k)
        self.assertEqual(is_3, 3)

    def testABAB(self) :
        k='ABAB'
        is_0 = alternatingCharacters(k)
        self.assertEqual(is_0, 0)
    
    