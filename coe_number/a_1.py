from itertools import combinations
def alternate(s):
    max_txt = 0
    for i, z in combinations(set(s), 2):
        on_string = ''.join((c for c in s if c==i or c==z))
        if i+i in on_string:
             continue

        if z+z in on_string: 
            continue
        max_txt = max((max_txt, len(on_string)))
    return max_txt